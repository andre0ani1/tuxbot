<?php


class Config
{

    public $log = false;
    public $multiChatbot = true;

    public $connectionInfo = array(
        "host" => "localhost",
        "user" => "",
        "pass" => "",
        "dbName" => ""
    );

    public $parserInfo = array(
        "aimlDir" => "aiml"
    );

    public $botInfo =  array(
            'name' => "Tux Bot",
            'age' => "1",
            'owner' => "Patrice",
            'website' => "https://andre-ani.fr",
            'version' => "1.0"
        );

    public $userInfo = array(
        "name" => "userName",
        "age" => 20,
    );

}
