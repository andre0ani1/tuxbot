﻿<!DOCTYPE html>
<html lang="fr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="web/favicon.ico" />
    <title>Tux The Bot</title>
    <script src="web/js/jquery.js" type="text/javascript"></script>

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="web/css/main.css" type="text/css" />

    <script type="text/javascript">
        $(document).ready(function() {

            var localUrl = window.location.href;
            localUrl = localUrl.replace('web/', '');

            var webServiceUrl = window.location.href + 'api.php';
            console.log(webServiceUrl);
            $('.clean').click(function() {

                Clear();
                AddText('system ', 'cleaning...');

                $('.userMessage').hide();

                $.ajax({
                    type: "GET",
                    url: webServiceUrl,
                    data: {
                        requestType: 'forget'
                    },
                    success: function(response) {
                        AddText('system ', 'Ok!');
                        $('.userMessage').show();
                    },
                    error: function(request, status, error) {
                        Clear();
                        alert('error');
                        $('.userMessage').show();
                    }
                });
            });


            $('#fMessage').submit(function() {

                // get user input
                var userInput = $('input[name="userInput"]').val();

                // basic check
                if (userInput == '')
                    return false;
                //

                // clear
                $('input[name="userInput"]').val('');

                // hide button
                $(this).hide();

                // show user input
                AddText('Vous ', userInput);

                $.ajax({
                    type: "GET",
                    url: webServiceUrl,
                    data: {
                        userInput: userInput,
                        requestType: 'talk'
                    },
                    success: function(response) {
                        console.log(webServiceUrl);
                        console.log(userInput);

                        AddText('Tux ', response.message);
                        $('#fMessage').show();
                        $('input[name="userInput"]').focus();
                    },
                    error: function(request, status, error) {
                        console.log(error);
                        alert('error');
                        $('#fMessage').show();
                    }
                });

                return false;
            });

            function Clear() {
                $('.chatBox').html('');
            }

            function AddText(user, message) {
                console.log(user);
                console.log(message);

                var div = $('<div>');

                if(user == 'Tux '){ div.addClass('bot'); }

                var name = $('<label>').addClass('name');
                var text = $('<span>').addClass('message');

                name.text(user + ':');
                text.html('\t' + message);

                div.append(name);
                div.append(text);

                $('.chatBox').append(div);

                $('.chatBox').scrollTop($(".chatBox").scrollTop() + 100);
            }


        });
    </script>
</head>

<body id="body">
    <div class="container-fluid">

    <div class="row">

                <div class="col-4">
                </div>

                <div class="col-4">
                    <p class="h2 tux p-3 mb-2 bg-info text-white">Tux Bot</p>
                    
                </div>

                <div class="col-4">
                </div>

            </div>

            <div class="row">
                <div class="col-2">
                </div>

                <div class="col-8">
                <br>
                    Bienvenue sur Tux Bot, un bot libre (en PHP et AIML) dont le but est d'informer et d'éduquer au logiciel libre.
                    Posez vos questions sur le libre et Linux.
                    Par exemple, qu'est-ce qu'une distribution Linux, qu'elles sont les licences libres, qui sont les acteurs du libre, ce genre de choses ;-)
                <br><br><br>
                </div>

                <div class="col-2">
                </div>

            </div>


            <div class="row">

                <div class="col-2 img-fluid text-center">
                    <img src="web/img/tux.png">
                </div>

                <div id="box1" class="col-8">
                    
                    <div class="chatBox">
                        Bienvenue, en quoi puis-je vous aider ?
                    </div>
                </div>

                <div class="col-2">
                </div>

            </div>



            <div class="row">

                <div class="col-4">
                </div>

                <div id="box2" class="col-4 userMessage">

                    <form id="fMessage">
                        <div class="form">


                            <div class="rep">
                                <input type="text" name="userInput" id="userInput" />
                            </div>
                            <div class="but">
                                <input id="clean" type="button" class="btn btn-secondary" value="Effacer" />
                                <input id="send" type="submit" value="Envoyer" class="btn btn-primary" />
                            </div>
                        </div>
                    </form>

                </div>

                <div class="col-4">
                </div>

            </div>

        </div>

</body>


</html>
